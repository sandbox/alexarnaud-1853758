var drupactouchcjq = jQuery.noConflict(true);
jQuery(document).ready(function($){
  drupactouchcjq("#custommenu").menu({ position: { my: "center top", at: "center bottom"} });
});

function showMainMenuEntry(entry) {
  var div = entry.find('div');
  if (div.length > 0) {
    var offset = entry.offset();
    var height = entry.height();

    var div_offset = {
      'top': offset.top + height,
      'left': offset.left
    };

    div.show();
    div.offset(div_offset);
  }
}

function hideMainMenuEntry(entry) {
  entry.find('div').hide();
}

jQuery(document).ready(function() {
  var dtjq = drupactouchcjq;
  dtjq("#main-menu > ul > li").bind('mouseover', function() {
    entry = dtjq(this);
    showMainMenuEntry(entry);
  });

  dtjq("#main-menu > ul > li").bind('mouseout', function() {
    entry = dtjq(this);
    hideMainMenuEntry(entry);
  });

  dtjq("#main-menu div.menu-content").hide();
});

<?php

/**
 * @file
 * Color module integration.
 */

// Put the logo path into JavaScript for the live preview.
drupal_add_js(array('color' => array('logo' => theme_get_setting('logo', 'drupac_touch'))), 'setting');

$info = array(
  // Available colors and color labels used in theme.
  'fields' => array(
    'text' => t('Text color'),
    'link' => t('Link color'),
    'base' => t('Base color'),
    'bg' => t('Main background'),
    'topmenubg' => t('Top menu background'),
    'headerbg' => t('Header background'),
    'navbg' => t('Navigation menu background'),
    'mainmenubg' => t('Main menu background'),
    'footerbg' => t('Footer background'),
    'topmenu' => t('Top menu links'),
    'header' => t('Header text'),
    'nav' => t('Navigation menu links'),
    'footer' => t('Footer links'),
  ),
  // Pre-defined color schemes.
  'schemes' => array(
    'default' => array(
      'title' => t('Default scheme'),
      'colors' => array(
        'text' => '#000000',
        'link' => '#0071b3',
        'base' => '#7f7f7f',
        'bg' => '#eeeeee',
        'topmenubg' => '#111111',
        'headerbg' => '#292928',
        'navbg' => '#292929',
        'mainmenubg' => '#404040',
        'footerbg' => '#888888',
        'topmenu' => '#9f9f9f',
        'header' => '#ffffff',
        'nav' => '#a0a0a0',
        'footer' => '#f0f0f0',
      ),
    ),
    'redgray' => array(
      'title' => t('Red and Gray'),
      'colors' => array(
        'text' => '#333333',
        'link' => '#cc0000',
        'base' => '#7f7f7f',
        'bg' => '#ffffff',
        'topmenubg' => '#ffffff',
        'headerbg' => '#aaaaaa',
        'navbg' => '#aaaaaa',
        'mainmenubg' => '#878787',
        'footerbg' => '#aaaaaa',
        'topmenu' => '#cc0000',
        'header' => '#333333',
        'nav' => '#333333',
        'footer' => '#cc0000',
      ),
    ),
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'css/colors.css',
  ),

  // Files to copy.
  'copy' => array(
    'logo.png',
  ),

  // Gradient definitions.
  'gradients' => array(),

  // Color areas to fill (x, y, width, height).
  'fill' => array(),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_css' => 'color/preview.css',
  'preview_js' => 'color/preview.js',
  'preview_html' => 'color/preview.html',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);

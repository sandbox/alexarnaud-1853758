<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

<?php
  // Retrieve search form settings.
  $search_form_base_url = theme_get_setting('search_form_base_url');
  $search_form_indexes = theme_get_setting('search_form_indexes');
  $search_form_additional_url_parameters = theme_get_setting('search_form_additional_url_parameters');
?>

<div id="page-wrapper">
  <div id="top-menu" role="navigation">
    <ul class="clearfix inline">
      <li>
        <?php if ($logged_in): ?>
          <a href="/user/logout"><?php print t('Log out') ?></a>
        <?php else: ?>
          <a href="/user/login"><?php print t('Log in') ?></a>
        <?php endif; ?>
      </li>
    </ul>
  </div>

  <div id="page">

    <!-- HEADER -->
    <div id="header">
      <div class="section">
        <!-- logo -->
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>

        <!-- site name and slogan -->
        <?php if ($site_name || $site_slogan): ?>
          <div id="name-and-slogan" class="clearfix">
            <?php if ($site_name): ?>
              <span id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </span>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
              <span id="site-slogan"><?php print $site_slogan; ?></span>
            <?php endif; ?>
          </div><!-- /#name-and-slogan -->
        <?php endif; ?>

        <!-- region header -->
        <?php print render($page['header']); ?>

      </div><!-- .section -->
    </div><!-- /#header -->

    <!-- navigation -->
    <div id="navigation">
      <div class="section">
        <?php if ($main_menu_tree): ?>
          <nav id="main-menu" role="navigation">
            <?php
              print theme('main_menu', array('tree' => $main_menu_tree));
            ?>
          </nav>
        <?php endif; ?>

        <div id="user-zone">
          <?php if ($page['user_menu']): ?>
            <div id="user_menu" class="user-menu">
              <?php print render($page['user_menu']); ?>
            </div><!-- /#user_menu -->
          <?php endif; ?>

          <?php if ($secondary_menu): ?>
            <ul id="custommenu">
              <li><a href="#"><?php print t('My Account'); ?></a>
            <?php print theme('links__system_secondary_menu', array(
              'links' => $secondary_menu,
              'attributes' => array(
                'id' => 'secondary-menu-links',
                'class' => array('links', 'inline', 'clearfix'),
              ),
              'heading' => array(
                'text' => t('Secondary menu'),
                'level' => 'h2',
                'class' => array('element-invisible'),
              ),
            )); ?>
              </li>
            </ul>
          <?php endif; ?>
        </div>
      </div>
    </div><!-- /#navigation -->

    <div id="main-wrapper" class="clearfix">
      <!-- MAIN -->
      <?php
        if ($page['sidebar_first'] && $page['sidebar_second']):
          $main_class = "two-sidebars";
        elseif ($page['sidebar_first'] || $page['sidebar_second']):
          $main_class = "one-sidebar";
        else:
          $main_class = "no-sidebar";
        endif
      ?>
      <div id="main" class="section <?php print $main_class; ?>">

        <?php if ($breadcrumb): ?>
          <div id="breadcrumb"><?php print $breadcrumb; ?></div>
        <?php endif; ?>

        <?php if ($page['sidebar_first']): ?>
          <div id="sidebar-first" class="column sidebar">
            <?php print render($page['sidebar_first']); ?>
          </div><!-- /#sidebar-first -->
        <?php endif; ?>

        <div id="content" class="column" role="main">
          <?php print render($page['highlighted']); ?>
          <a id="main-content"></a>
          <?php print render($title_prefix); ?>
          <?php if ($title): ?>
            <h1 class="title" id="page-title"><?php print $title; ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
          <?php print $messages; ?>
          <?php print render($tabs); ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
          <?php print render($page['content']); ?>
          <?php print $feed_icons; ?>
        </div><!-- /#content -->

        <?php if ($page['sidebar_second']): ?>
          <div id="sidebar-second" class="column sidebar">
            <?php print render($page['sidebar_second']); ?>
          </div> <!-- /#sidebar-second -->
        <?php endif; ?>

      </div><!-- /#main -->
    </div><!-- /#main-wrapper -->

    <?php if ($page['footer']): ?>
      <div id="footer">
        <?php print render($page['footer']); ?>
      </div> <!-- /#footer -->
    <?php endif; ?>

  </div> <!-- /#page -->
</div> <!-- /#page-wrapper -->

<script type="text/javascript">
//<![CDATA[

  jQuery(document).ready(function($) {
    $("#search-form").submit(function() {
      $('#search-form .search-form-index-url-parameters').empty();
      var url_parameters = eval($('#search-form-indexes').find('option:selected').val());
      for (i in url_parameters) {
        parameter = url_parameters[i]
        if (parameter.name.length > 0) {
          if (parameter.value == '[search]') {
            // Replace by text in search box
            parameter.value = $('#header-search-text').val();
          }
          $('#search-form').append('<input type="hidden" name="'+parameter.name+'" value="'+parameter.value+'" />');
        }
      }
      return true;
    });
  });

//]]>
</script>

<?php

/**
 * @file
 * Theme settings form.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function drupac_touch_form_system_theme_settings_alter(&$form, $form_state) {
  $form['search_form_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search form settings'),
  );
  $form['search_form_settings']['search_form_base_url'] = array(
    '#type' => 'textfield',
    '#title' => 'Base URL',
    '#default_value' => theme_get_setting('search_form_base_url'),
  );

  $form['search_form_settings']['search_form_indexes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Indexes'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $indexes = theme_get_setting('search_form_indexes');
  $i = 0;
  if (isset($indexes)) {
    foreach ($indexes as $index) {
      if ($index['label'] != '') {
        $form['search_form_settings']['search_form_indexes'][$i]
          = _drupac_touch_form_index_element($i, $index['label'],
            $index['url_parameters']);
        $i++;
      }
    }
  }
  $form['search_form_settings']['search_form_indexes'][$i]
    = _drupac_touch_form_index_element($i);

  $form['search_form_settings']['search_form_additional_url_parameters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additional URL parameters'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $additional_url_parameters = theme_get_setting('search_form_additional_url_parameters');
  $i = 0;
  if (isset($additional_url_parameters)) {
    foreach ($additional_url_parameters as $url_parameter) {
      if ($url_parameter['name'] != '') {
        $form['search_form_settings']['search_form_additional_url_parameters'][$i]
          = _drupac_touch_form_url_parameter_element($i, $url_parameter['name'],
            $url_parameter['value']);

        $i++;
      }
    }
  }
  $form['search_form_settings']['search_form_additional_url_parameters'][$i]
    = _drupac_touch_form_url_parameter_element($i);

  return $form;
}

/**
 * Build all the form elements for a single index.
 */
function _drupac_touch_form_index_element($delta, $label = NULL, $url_parameters = array()) {
  $new = isset($label) ? FALSE : TRUE;
  $form = array(
    '#type' => 'fieldset',
    '#title' => $new ? t('New index') : $label,
    '#collapsible' => TRUE,
    '#collapsed' => $new ? FALSE : TRUE,
  );
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $label,
  );

  $form['url_parameters'] = array(
    '#type' => 'fieldset',
    '#title' => t('URL parameters'),
    '#collapsible' => TRUE,
  );

  $i = 0;
  foreach ($url_parameters as $url_parameter) {
    if ($url_parameter['name'] != '') {
      $form['url_parameters'][$i]
        = _drupac_touch_form_index_url_parameter_element($delta, $i,
          $url_parameter['name'], $url_parameter['value']);
      $i++;
    }
  }
  $form['url_parameters'][$i]
    = _drupac_touch_form_index_url_parameter_element($delta, $i);

  return $form;
}

/**
 * Build all the form elements for a single index URL parameter.
 */
function _drupac_touch_form_index_url_parameter_element($index, $delta, $name = NULL, $value = NULL) {
  $new = isset($name) ? FALSE : TRUE;
  return array(
    '#type' => 'fieldset',
    '#title' => $new ? t('New parameter') : $name,
    '#collapsible' => TRUE,
    '#collapsed' => $new ? FALSE : TRUE,

    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Parameter name'),
      '#default_value' => $name,
    ),

    'value' => array(
      '#type' => 'textfield',
      '#title' => t('Parameter value'),
      '#default_value' => $value,
    ),
  );
}

/**
 * Build all the form elements for a single additional url parameter.
 */
function _drupac_touch_form_url_parameter_element($delta, $name = NULL, $value = NULL) {
  $new = isset($name) ? FALSE : TRUE;
  return array(
    '#type' => 'fieldset',
    '#title' => $new ? t('New parameter') : $name,
    '#collapsible' => TRUE,
    '#collapsed' => $new ? FALSE : TRUE,

    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Parameter name'),
      '#default_value' => $name,
    ),

    'value' => array(
      '#type' => 'textfield',
      '#title' => t('Parameter value'),
      '#default_value' => $value,
    ),
  );
}

<?php

/**
 * @file
 * Main theme file.
 */

/**
 * Implements hook_preprocess_page().
 */
function drupac_touch_preprocess_page(&$vars) {
  $main_menu = menu_build_tree('main-menu', array(
    'max_depth' => 4,
  ));
  $vars['main_menu_tree'] = $main_menu;
}

/**
 * Implements hook_process_html().
 */
function drupac_touch_process_html(&$vars) {
  if (module_exists('color')) {
    _color_html_alter($vars);
  }
}

/**
 * Implements hook_process_page().
 */
function drupac_touch_process_page(&$vars) {
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

/**
 * Implements hook_theme().
 */
function drupac_touch_theme($existing, $type, $theme, $path) {
  return array(
    'main_menu' => array(
      'variables' => array(
        'tree' => array(),
      ),
    ),
  );
}

/**
 * Render the main menu tree as HTML.
 */
function drupac_touch_build_main_menu($tree, $depth = 0) {
  $menu_html = '';
  if ($depth == 1):
    $menu_html .= '<div class="menu-content">';
  endif;
  $menu_html .= '<ul>';
  foreach ($tree as $menu_entry):
    $link = $menu_entry['link'];
    if (!$link['hidden']) {
      $mlid = $link['mlid'];
      $class = "menu-$mlid";
      $title = $link['title'];
      $href = $link['href'];
      $href = preg_replace('/^<front>$/', '', $href);
      $href = "/$href";

      $menu_html .= "<li class=\"$class\">";
      $menu_html .= "<a href=\"$href\">$title</a>";
      if ($menu_entry['below']):
        $menu_html .= drupac_touch_build_main_menu($menu_entry['below'], $depth + 1);
      endif;

      $menu_html .= "</li>";
    }
  endforeach;
  $menu_html .= '</ul>';
  if ($depth == 1):
    $menu_html .= '</div>';
  endif;

  return $menu_html;
}

/**
 * Theme main_menu.
 */
function drupac_touch_main_menu($variables) {
  return drupac_touch_build_main_menu($variables['tree']);
}
